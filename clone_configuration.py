import logging

import plac

from kbcstorage.components import Components


class ConfigDuplicator:
    def __init__(self, client_from: Components, client_to: Components):
        """
        Initialise a client.
        Args:
            client_from (Components): Client object for source configuration.
            client_to (Components): Client object for destination configuration.

        """

        self.client_from = client_from
        self.client_to = client_to

    def migrate_configs(self, src_config_id, component_id):
        """
        Super simple method, getting all table config objects and updating/creating them in the destination configuration.
        Includes all attributes, even the ones that are not updateble => API service will ignore them.

        """
        src_config = self.client_from.get_config_detail(component_id, src_config_id)

        src_config_rows = self.client_from.get_config_rows(component_id, src_config_id)

        dst_config = src_config.copy()
        # add component id
        dst_config['component_id'] = 'transformation'

        logging.info('Transfering config..')
        new_cfg = self.client_to.create(**dst_config)

        logging.info('Transfering config rows')
        for row in src_config_rows:
            row['component_id'] = component_id
            row['configuration_id'] = new_cfg['id']
            test=row['configuration'].pop('id')
            test =row['configuration'].pop('rowId',{})
            test =row.pop('rowId',{})

            self.client_to.create_config_row(**row)


def main(from_config, from_token, to_token, component_id, storage_api_url_from='https://connection.keboola.com',
         storage_api_url_to='https://connection.keboola.com'):
    """
    Simple CMD to migrate component config from one config to another. Even across projects

    """
    from_client = Components(storage_api_url_from, from_token)
    to_client = Components(storage_api_url_to, to_token)

    duplicator = ConfigDuplicator(from_client, to_client)

    logging.info('Migrating component %s configuration from config ID: %s ', component_id, from_config)
    try:
        duplicator.migrate_configs(from_config, component_id)
    except Exception as err:
        logging.error("Failed to migrate the configuration: %s", err)
        exit()


if __name__ == '__main__':
    plac.call(main)
